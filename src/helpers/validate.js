const validate = (values) => {
  const errors = {};
/* validating full name */
  if (!values.fullName) {
    errors.fullName = "Full name is required";
  } else if (values.fullName.length < 5) {
    errors.fullName = "Full name should have min 5 characters";
  }
/* validating email using regex to pass email */
  if (!values.email) {
    errors.email = "Email address is required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

/* validating passwords */
  if (!values.password) {
    errors.password = "Password is required";
  } else if (!/^(?=(.*[a-z]){2})(?=(.*[A-Z]){2})(?=.*\d)/i.test(values.password)) {
    errors.password = "Password should have atleast 2 uppercase letter, 2 lowercase letter & 1 number. (eg. F22Labs)";
  }

  return errors;
};

export default validate;