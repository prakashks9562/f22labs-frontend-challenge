// Open the URLs in browser to see the response JSON.
// You have 4 different member APIs here
// Use any one member API to build the page
// Once the UI is completed, check with all the remaining APIs to ensure the UI isnt broken

const ricardoCooper = {
  userProfile:
    "https://jsonblob.com/api/jsonBlob/899671370464772096",
  teamList:
    "https://jsonblob.com/api/jsonBlob/899670589053353984",
};

const courtneyHenry = {
  userProfile:
    "https://jsonblob.com/api/jsonBlob/899671012145381376",
  teamList:
    "https://jsonblob.com/api/jsonBlob/899669532994715648",
};

const jennyWilson = {
  userProfile:
    "https://jsonblob.com/api/jsonBlob/899671197336485888",
  teamList:
    "https://jsonblob.com/api/jsonBlob/899670441082503168",
};

const timothéeNaël = {
  userProfile:
    "https://jsonblob.com/api/jsonBlob/899672062428463104",
  teamList:
    "https://jsonblob.com/api/jsonBlob/899670824634826752",
};

export { ricardoCooper, courtneyHenry, jennyWilson, timothéeNaël };
