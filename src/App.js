import React from "react";
import "./App.css";
import LandingPage from "./pages/LandingPage";
import Signup from "./pages/Signup";
import Home from "./pages/Home";
import { Switch, Route } from "react-router-dom";

const App = () => {
  return (
    <div>
    
      {/*  

        # TASK

        Points: 4

        Create 2 new routes
          1. Landing route will be / which renders LandingPage component (pages/LandingPage)
          2. /signup route will render Signup component (pages/Signup)
          3. /home route will render Home component (pages/Home)

      */}
      <Switch>
        <Route path="/signup" component={Signup} />
        <Route path="/home" component={Home} />
        <Route path="" component={LandingPage} />
      </Switch>
    </div>
  );
};

export default App;
